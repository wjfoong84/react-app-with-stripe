import 'bootstrap/dist/css/bootstrap.css';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';

import { useState } from 'react';

function App() {
  const [productName, setProductName] = useState(''); 
  const [productDesc, setProductDesc] = useState(''); 
  const [productCurrency, setProductCurrency] = useState(''); 
  const [productPrice, setProductPrice] = useState('0.0'); 


  function onSubmitHandler(event) {
    event.preventDefault();

    // console.log('API KEY' + process.env.REACT_APP_STRIPE_API_KEY);
    console.log(productName + ' - ' + productDesc + ' - ' + productCurrency + ' - ' + productPrice);
    const stripe = require('stripe')(process.env.REACT_APP_STRIPE_API_KEY);

    stripe.products.create({
      name: productName,
      description: productDesc,
    }).then(product => {
      stripe.prices.create({
        unit_amount: productPrice,
        currency: productCurrency,
        recurring: {
          interval: 'month',
        },
        product: product.id,
      }).then(price => {
        console.log('Success! Here is your starter subscription product id: ' + product.id);
        console.log('Success! Here is your starter subscription price id: ' + price.id);
      });
    });
  }

  return (
    <Form className='container text-center' onSubmit={onSubmitHandler}>
      <h1>Create Product</h1>

      <Row className='mb-3'>
        <Col sm={12}>
          <Form.Control required placeholder="Name" value={productName} onChange={e => setProductName(e.target.value)} /> 
        </Col>
      </Row>
  
      <Row className='mb-3'>
        <Col sm={12}>
          <Form.Control placeholder='Description' value={productDesc} onChange={e => setProductDesc(e.target.value)} />
        </Col>
      </Row>
      
      <Row className='mb-3'>
        <Col sm={2}>
          <Form.Select aria-label="Currency" value={productCurrency} onChange={e => setProductCurrency(e.target.value)}>
            <option>Currency</option>
            <option value="usd">USD$</option>
            <option value="sgd">SGD$</option>
            <option value="myr">RM</option>
          </Form.Select>
        </Col>

        <Col sm={10}>
          <Form.Control type="number" placeholder="Price" value={productPrice} onChange={e => setProductPrice(e.target.value)} />
        </Col>
      </Row>

      <Row className="mb-3">
        <Col sm={12}>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Col>
      </Row>
    </Form>
  );
}

export default App;
